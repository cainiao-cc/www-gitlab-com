(function(){

  var nav = document.getElementById('main-nav');
  var rotatingWord = document.getElementById('rotating-word').firstElementChild;

  var words = [
    'To software modernization',
    'To faster cycle times',
    'To secure apps',
    'To a single application for the entire DevOps lifecycle'
  ]

  var animationDuration = 3;

  function rotateWords() {
    rotatingWord.innerHTML = words[0]
    var index = 1;
    setInterval(() => {
      rotatingWord.innerHTML = words[index]
      index += 1
      if (index >= words.length) {
        index = 0;
      }
    }, animationDuration * 1000);
  }

  function changeNavClass() {
    if (window.scrollY >= 1) {
      nav.classList.remove('navbar-header-transparent');
      nav.classList.add('navbar-header-dark');
    } else {
      nav.classList.remove('navbar-header-dark');
      nav.classList.add('navbar-header-transparent');
    }
  }

  rotateWords();

  window.addEventListener('scroll', changeNavClass);

})();
