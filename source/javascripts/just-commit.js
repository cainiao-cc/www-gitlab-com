(function(){
  var nav = document.getElementById('main-nav');
  var hero = document.getElementById('just-commit-hero');
  var commitGrid;
  var bigStats = document.getElementById('big-stats');
  var movingLines = document.getElementById('moving-lines');

  var commitBoxColors = [
    '#9795F9',
    '#6262CC',
    '#3F3177',
    '#272359',
    '#ffffff'
  ]

  // animation variables
  var commitRows = 10;
  var commitBoxes = 20;

  function createCommitGridRows() {
    var rowBoxDelay = 0;

    // create row
    for (var i = 0; i < commitRows; i++) {
      var commitRow = document.createElement('DIV');
      var boxDelay = 0;

      // create boxes for each row
      for (var j = 0; j < commitBoxes; j++) {
        var randomNumber = Math.floor(Math.random() * commitBoxColors.length);
        var commitBox = document.createElement('DIV');
        commitBox.style.background = commitBoxColors[randomNumber]
        commitBox.classList.add('commit-box');
        commitBox.style.animationDelay = rowBoxDelay + boxDelay + 's';

        boxDelay += 1;

        // append all boxes to each row
        commitRow.insertBefore(commitBox, commitRow.children[0])
        // commitRow.appendChild(commitBox);
      }

      rowBoxDelay = Math.floor(Math.random() * commitRows);

      // append rows with boxes to commit grid
      commitRow.classList.add('commit-row');
      commitGrid.insertBefore(commitRow, commitGrid.children[0])
      // commitGrid.appendChild(commitRow);
    }

    commitGrid.style.top = 'calc(50% - ' + commitGrid.offsetHeight/2 + 'px )'
  }

  function createCommitGrid() {
    commitGrid = document.createElement('DIV');
    commitGrid.classList.add('commit-grid');
    hero.appendChild(commitGrid);

    createCommitGridRows()
  }

  function changeNavClass() {
    if (movingLines) {
      if (movingLines.offsetTop - window.scrollY <= window.innerHeight) {
        movingLines.classList.add('full-width-content')
      } else {
        movingLines.classList.remove('full-width-content')
      }
    }
    if (bigStats) {
      if (bigStats.offsetTop - window.scrollY <= window.innerHeight/2) {
        bigStats.children[0].firstElementChild.style.width = "calc(96% - 8px)";
        bigStats.children[1].firstElementChild.style.width = "calc(57% - 8px)";
        bigStats.children[2].firstElementChild.style.width = "calc(78% - 8px)";
      } else {
        bigStats.children[0].firstElementChild.style.width = "0";
        bigStats.children[1].firstElementChild.style.width = "0";
        bigStats.children[2].firstElementChild.style.width = "0";
      }
    }
    if (window.scrollY >= 1) {
      nav.classList.remove('navbar-header-transparent');
      nav.classList.add('navbar-header-dark');
    } else {
      nav.classList.remove('navbar-header-dark');
      nav.classList.add('navbar-header-transparent');
    }
  }

  createCommitGrid();

  window.addEventListener('scroll', changeNavClass);
})();
