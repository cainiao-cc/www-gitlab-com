---
layout: handbook-page-toc
title: "Customer Health Assessment and Triage"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

----

This page covers the factors to consider for customer health, guidelines for selecting the appropriate rating, communication guidelines, and instructions for the account triage issue creation.

In order to manage customers at scale we must have a consistent way of keeping track of customer details including the current “health” of the customer. The health of a customer is affected by many different factors including product usage, support interactions, internal budgets, and TAM communication.  

The current health of a customer is also directly related to the customer’s journey with GitLab. Because of this, customer health and the running communication notes between a TAM and the customer should be closely linked.

## Customer Cadence

One of the primary tools TAMs have to assess account health, and take steps to improve a weaker health score, is the customer cadence. This is an opportunity for the TAM (and other GitLab team members that the TAM feels should be included) and the customer team to sync on business outcomes, priorities, progress on initiatives, and concerns.

### Enterprise

Customer cadence calls should be scheduled weekly during the customer onboarding phase and at least monthly otherwise. TAM Sentiment and Product Risk fields should be updated at the end of each cadence call as part of the Timeline entry.

### Mid-Market

Customer cadence calls should be scheduled frequently during the customer onboarding phase and at least quarterly otherwise. TAM Sentiment and Product Risk fields should be updated at the end of each cadence call as part of the Timeline entry.

## Timeline Notes

Meeting/call notes are the running notes for the TAM’s regular interaction with the customer. The call notes should at least cover the following topics:

- Updates on action items from the TAM
- New updates from the customer
  - Architectural changes
  - New deployments
  - Product feedback
- A review of any open support tickets in Zendesk
- Q&A
- Current customer health from a business and operational standpoint

At the end of each customer call any changes to customer health should be reflected in the customer's [Timeline in Gainsight](/handbook/customer-success/using-gainsight-within-customer-success/). You have a few ways to update the TAM Sentiment and Product Sentiment for an account's health score, described [below](/handbook/customer-success/tam/health-score-triage/#determining-tam-sentiment-and-product-risk).

If the Customer Health Score drops to Yellow or below, [an issue must be created in the Account Triage Project](#account-triage-project).

In addition to Timelines notes in Gainsight, call notes should be [saved in Google Drive following this format](https://drive.google.com/drive/folders/0B-ytP5bMib9Ta25aSi13Q25GY1U):

`/Sales/Customers & Prospects/A/Acme/Acme - Meeting Notes`

[See an example meeting notes here](https://docs.google.com/document/d/1dAcHBqoRTY6qqSw27VQstCCnk5Fxc2oIsbpKs014h3g).

The rationale for saving call notes in this manner is as follows:

- Call notes frequently contain sensitive information and are for the internal sales team and management to review.
- Call notes are tightly linked to the Health Score and should be available in the same “pane of glass” as the Health Score.
- Access to Gainsight is limited to TAMs, so other members of the Sales and Customer Success organizations will look for notes in Google Drive.
- A folder structure allows non-Customer Success executives and support staff to easily locate the notes in the case of an escalation.
- The naming convention “&lt;customer&gt; - Meeting Notes” allows for fast searching using [Google Cloud Search for Work](https://cloudsearch.google.com/)

## Customer Health Assessment

The following guideline will provide Technical Account Managers (TAMs) guidance to choose the right health assessment for her/his customer account. Health considers both delivery of value and outcomes to customers and business impact to GitLab.

The two fields TAMs update are TAM Sentiment and Product Sentiment, with each having a different weight in determining overall account health. While TAM Sentiment and Product Risk are highly related and complementary and share much of the guidelines below, the definitions for each are as follows:

- **TAM Sentiment**: Manual measure that the TAM updates to indicate their perceived sentiment of the customer
- **Product Sentiment**: Manual measure updated by the TAM indicating risk of downsell or churn due to product expectations

To update a customer's TAM Sentiment and Product Sentiment:

1. Update it directly from the customer list on your dashboard (scroll to the right if you don't see them)
1. Update it from the account's scorecard
1. Update it while logging an activity/timeline event (call, etc.)

If you would like to add a note corresponding to the health score (recommended), go to the account's scorecard and click on the TAM or Product Sentiment. Then click the plus button, choose "Update" or "Call", then input your notes and log.

Please note that if you update a health score from logging an activity (such as in the above example), the change will not be reflected for the account until the following day. If you change it directly from your dashboard or the account's scorecard, it will update immediately.

You will receive CTAs automatically to update health scores if they are out of date (monthly for Enterprise accounts and quarterly for Commercial).

There are a number of [enablement videos](/handbook/customer-success/using-gainsight-within-customer-success/#videos) you can watch to learn how to update customer health assessment and log activities that affect that assessment.

### Determining TAM Sentiment and Product Risk

Some common things to look out for during regular meetings are:

- **Utilization of the product.** Is it just being used for a single facet of the GitLab experience (e.g. SCM only)?
- **High number of “important” feature requests.** Is the customer trying to make GitLab work just like another legacy piece of software in their environment? A review of the GitLab workflow and best practices may be in order.
- **High number of support requests.** Is the customer struggling to keep GitLab operational or performant? An architecture review should be considered.
- **Missing cadence calls.** Missing cadence calls could be a sign that GitLab is being deprioritized, work with your SAL to schedule a [EBR](/handbook/customer-success/tam/ebr) or higher level discussion to evaluate the customer’s health.
- **Failure to upgrade.** Is the customer more than a major release behind? Work with the customer to explain the new features and security updates of the current version of GitLab and address any internal technical limitations that have prevented upgrades. Develop an upgrade plan with the customer if appropriate.

There are additional health score criteria that are automatically created and do not require manual updating, including:

- Deployment/Upgrades
- Engagement
- ROI
- Support Issues

### Risk Factors to Consider

The following are factors when considering product health, noting specific examples of things that could bring risk to customer health.

- **Product adoption**: There is a delayed, low or materially reduced usage (i.e., drop in usage) as measured by license consumption, features / use cases, product version (self-managed only), and/or GitLab stages. Value and outcome delivery to the customer misses expectations.
- **Product experience**: Customer has enhancements or defect fixes that are necessary for a customer and measured by the criticality of the request, severity of the issues and/or number of enhancements and defects. Missed expectations for feature release can also impact product experience.
- **Lack of customer engagement**: Customer contact(s) are not responsive, miss meetings and/or unwilling to engage in cadence calls or other engagements like [EBRs](/handbook/customer-success/tam/ebr/).
- **Loss of executive sponsor or champion**: Sponsor or champion leaves the company, moves to a different part of the organization and/or has reduced scope of influence
- **Low customer sentiment**:  The customer has expressed concerns and/or dissatisfaction with their experiences with GitLab (i.e., sales, professional services, support, product, etc.) through direct conversations, surveys (e.g., NPS), social media or other communication channels.
- **Other organizational factors**: The customer's business performance is materially impacted and declining. The company is acquired, merging with another company, divested or another structural change to customer's business.

### Health Score Rating

The items below serve as _guidelines_ for the TAM to score the customer. They are not intended to be a strict or rigid set of criteria.

#### Green

**Engagement, adoption and experiences are as expected or better than expected:**

- Successful GitLab use-cases (Success Plans).
- Uses the full GitLab DevOps lifecycle.
- Regular communication and positive relationship.
- Healthy user adoption rate.
- Regularly show interest and utilisation in new features.
- Always show up to regular cadence calls and communicates when they can't.
- They are interested in upgrading.
- Up to date on releases.
- Raises between 1 and 5 tickets a month and regularly contributes to issues.
- Regularly provides feedback on how to improve GitLab.

#### Yellow

**Engagement, adoption and/or experiences are lower than expected:**

- Lack of response to us and/ or we haven’t done much discovery.
- Don’t have the right contacts for each key person.
- Only one primary stakeholder.
- At least one major release behind.
- Doesn't raise any tickets or contribute to any issues.
- Occasionally doesn't show up or are late to scheduled cadence calls.
- Customer doesn't know what success looks like or it is ill-defined.
- Raises more than 10 tickets a month.

#### Red

**Engagement, adoption and/or experiences are significantly lower than expected:**

- They’ve told us they are downgrading/cancelling.
- No communication.
- Limited or no access to executive sponsors.
- Low user adoption.
- Loss of our GitLab champion.
- Original GitLab use-cases not achieved (Success Plan).
- Customer was acquired and parent company uses competitor.
- Complaints about the company (responsiveness, feature turnaround, missed priorities in direction).

### Communication Guidelines

The following are guidelines on who to notify when an account is yellow or red. Please make sure the following people are notified with the respective customer health ratings.

#### Yellow Health Rating

- Account Team (i.e., Strategic Account Leader or Account Executive, Solution Architect)
- Regional TAM Manager
- TAM Director (all non-Public Sector customers) or Director of Customer Success Public Sector (for Public Sector customers)

#### Red Health Rating

- Include the list above as well as...
- Area Sales Manager and Regional Director
- Vice President of Customer Success

### Responsibilities

The TAM is responsible for coordinating with all relevant parties to develop a plan to address the risks. Typically, this will involve the account team and communication group (above), as well as other resources such as Product Managers, marketing, executive or engineering resources meeting to develop and deliver the plan to address the risks. The TAM then drives execution of the strategy and is responsible for regular updates to the triage issue. When the risks have been addressed bringing the customer to a healthy / green status, the triage issue can be closed.

## Account Triage Project

An account risk issue should be created in the [Account Triage Project](https://gitlab.com/gitlab-com/customer-success/account-triage) if the customer health assessment is either yellow or red. These are also viewable in the [TAM Risk Account Issue Board.](https://gitlab.com/gitlab-com/customer-success/account-triage/-/boards/703769)

### Issue Template

When creating an issue in the [Account Triage](https://gitlab.com/gitlab-com/customer-success/account-triage) project, the default template will set up the details you should include. Follow the instructions laid out in the issue description by the template for what information should be documented in the issue.

### Issue Classification Labels

- `~E&A` Expected and Avoidable
- `~E&U` Expected and Unavoidable
- `~U&A` Unexpected and Avoidable
- `~U&U` Unexpected and Unavoidable

### Risk Labels

- `~HS::Green`: Green Health Rating  
- `~HS::Yellow`: Yellow Health Rating  
- `~HS::Red`: Red Health Rating  

### Region Labels

- `~US-WEST`
- `~US-EAST`
- `~EMEA`
- `~APAC`
- `~LATAM`

## Related Processes

[Customer Success Escalations Process](https://about.gitlab.com/handbook/customer-success/tam/escalations/)
