---
layout: handbook-page-toc
title: "DM.2.01 - Terms of Service Control Guidance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# DM.2.01 - Terms of Service

## Control Statement

Each new GitLab client must agree to the GitLab website Terms of Service which define the scope of services being provided including privacy and confidentiality requirements.

## Context

One of the purposes of a ToS is to provide users specific information about what personal information GitLab collects and alert them when that agreement is changed. This promotes transparency and allows users to make informed choices. The purpose of this control is to ensure the ToS includes information on what personal information is collected and a mechanism to alert users of any changes is in place.

## Scope

This control applies to GitLab's ToS.

## Ownership

Legal

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Terms of Service control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/793).

### Policy Reference

[Terms](https://about.gitlab.com/terms/)

## Framework Mapping

* SOC2 CC
  * CC2.3
