---
layout: handbook-page-toc
title: Understanding SOCIAL STYLES 
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introducing SOCIAL STYLES

On this page, we will detail a unique approach developed by the [TRACOM Corporation](https://tracom.com/social-style-training/model#) called SOCIAL STYLES&reg; that can be used by Managers and Individual Contributors. It is a framwork to improve interpersonal effectivess and [emotional intelligence](https://about.gitlab.com/handbook/people-group/learning-and-development/emotional-intelligence/) across remote teams at GitLab. As a Manager, you can be more effective in your relationships with other people if you learn to focus on their behavior in an objective fashion. SOCIAL STYLES is a structured approach for observing, understanding, and anticipating an individual's behavior. Managing in an all-remote company comes down to trust, communication, and [company-wide support of shared goals](https://about.gitlab.com/company/culture/all-remote/management/). Read this page to understand each one of your teammates unique SOCIAL STYLE. 

## Copyright/Trademark Recognition Statement
Used with permission of TRACOM Group. All rights reserved. SOCIAL STYLE is a service mark of TRACOM GROUP. The SOCIAL STYLE MODEL is a registered trademork of TRACOM Group. The phrase SOCIAL STYLEs is required to be capaitalized when used due to copyright rules. 

## Why we need SOCIAL STYLES

As a Manager at GitLab, SOCIAL STYLEs can help strengthen relationships, improve communication, and to predict behavior. As a people leader, you can develop a customized approach to each one of your team members based on their assessed style. This will enable personalization for Managers and Individual Contributors. It is an excellent tool for improving emotional intelligence and building relationships with your team.

>  "The more I learn about myself, the more I am able to understand others, the more I am able to bridge the gap between us."

**Benefits of SOCIAL STYLES:**
*  Strengthens relationships
*  Improves communication 
*  Helps predict behavior
*  Enables a personalized management approach for direct reports
*  Gives insight into how teammates operate

The one thing that all teams have in common is individuals. No matter what kind of team you are in, you will have to interact with your fellow team memebrs. SOCIAL STYLES can help you understand your team and how they are likely to interact with one another, and with you. 

## Managing with style

What seperates great managers from good ones is the ability to treat each team member as individuals while focusing on their unique strengths and areas of improvement. People who have different styles have different preferences and ways of working. When managed ineffectively, these differences can be the source of dissatisfaction, conflict, and a breakdown in team effectiveness. However, when managed with skill, team members style differences can lead to creativity, focus, energy, and better performance. A manager can handle team interpersonal interactions by applying the SOCIAL STYLES framework so that the performance of the team can be enhanced. 

## Scales of behavior and the four styles

In the SOCIAL STYLE Model their are two scales of behavior: 

1.  **Assertiveness:**
    *  Dimension of behavior that measures the degree to which others perceive a person as tending to ask or tell in interctions with others. (Ask versus Tell)
2. **Responsiveness:**
    *  Dimension of behavior that measures the degree to which others perceive a person as tending to control or display their feelings and emotions when interacting. (Control versus Emotes)

### The four styles

There are four styles related to the SOCIAL STYLE model. Each style represents a particular patter of actions that others can observe and agree upon for describing one's behavior. 

*  Driving: strong willed and more emotionally controlled
*  Expressive: outgoing and energizing
*  Amiable: easy going and supportive
*  Analytical: serious and more exacting 

**The SOCIAL STYLES Model&trade;** below categorizes the four unique styles: **Analytical**, **Driver**, **Expressive**, and **Amiable**. Team members can identify what style they are and adapt their approaches and communication style accordingly to their team. 

![SOCIAL STYLES Model](source/images/Social_Styles_Model.PNG)

## The four styles and how to handle 

<details>
  <summary markdown='span'>
    The Driving Style 
  </summary>

Team members that are classified as Drivers are action oriented that perfer fast-paced and structured enviornments.

**Key characteristics of a Driver:**

* Focus on the immediate timeframe
* Know what they want
* Be swift, efficent and to the point
* Be impatient with delays
* Interested in results and speed
* Tomorrow timeframe

**How to handle the Driver**

* Work hard & fast for goals
* Keep meetings short
* Provide leadership & challenges
* Expect competition
* Gain respect
* Expect critical reviews
* Be precise but quick - provide material/details to decide on

**Sample Driver Quote:** 

>  "I trust in myself and my skills, results is what counts for me."

</details>

<details>
  <summary markdown='span'>
    The Analytical Style 
  </summary>

Team members that are classified as Analytical are oriented towards thinking and prefer to live life according to facts, principles, logic, and consistency.

**Key characteristics of an Analytical**

* Risk-adverse and cautious
* Structured and want to see the detail
* Historic timeframe 
* Want to see the how
* Can be reluctant to declare a point of view
* Focus on processes and procedures
* Slow to change

**How to handle the Analytical**

* Don't expect to see emotions
* Avoid chaos
* Provide tasks with detail, patience, and analysis
* Lead back from detail to result
* For decisions provide details, alternatives, and precedent
* Keep meetings short
* Provide actions with conviction

**Sample Analytical Quote:** 

>  "I trust in figures, data, facts, and my own knowledge. I believe in thought-out decisions and actions."

</details>

<details>
  <summary markdown='span'>
    The Amiable Style 
  </summary>

Team members that are classified as Amiable are oriented towards relationships and tend to interpret the world on a personal basis and get involved in the feelings and relationships between people.

**Key characteristics of an Amiable**

* Prefer to get things done with and through others
* Risk-adverse and slower paced
* Very relationship oriented and harmony/consensus seeking
* Look for personal motives in the actions of others
* Sensitive to others and good team players
* Slow or reluctant to change
* Add joy, warmth, and freshness to social situations
* Easy for others to communicate and share with 

**How to handle the Amiable**

* Provide routines and structure
* Avoid neccessity to decide instantly
* Provide pleasurable and secure feeling with matters at hand
* Provide clear instructions
* Avoid risks and ambiguous situations
* Make clear you are not going to flee 

**Sample Amiable Quote:** 

>  "I lean towards the power of collaboration. I trust in the skills of others and the good in every person."

</details>

<details>
  <summary markdown='span'>
    The Expressive Style 
  </summary>

Team members that are classified as Expressive are oriented towards spontaneity. They tend to focus their attention on the future with intuitive visions and they want to be appreciated.

**Key characteristics of an Expressive**

* Imaginative and creative
* Make decisions quickly based on feelings
* Warm and approachable yet seeks recognition
* Generate enthusiasm 
* Behave in stimulating, exciting, and fun ways
* Can make mistakes and have frequent changes in direction and focus due to their desire to act on opinions, hunches, and intuitions versus facts and data
* Prefers faster-paced and unstructured environments 
* Focused on innovation and vision with a future timeframe

**How to handle the Expressive**

* Be open-minded
* Give feedback and show appreciation
* Avoid skeptical statements
* Avoid tasks with too much detail and implementation
* Foster a team atmosphere
* Expect/provide distractions
* Provide opportunities to be talented and resourceful

**Sample Expressive Quote:** 

>  "I lean on the power of visions, I trust in my persuasive power."

</details>

## Working with other styles

Below are some additional strategies for Managers to adapt and personalize their management style for your teams individual SOCIAL STYLE. Once you know the styles of your team you can tailor interactions and communications with them to strengthen relationships. 

The left hand side represents your style while the top represents your counterparts. Alter your style when interacting with your team members and leading others. 


|     | **Driving** | **Expressive** | **Amiable** | **Analytical** |
| ------ | ------ | ------ | ------ | ------ |
| **Driving** | Maintain peer status, share the resonsibilities of leadership | Shift your focus to a more general outlook, be less formal | Slow your pace and focus more on the relationships | Focus on some detail in relation to the goals |
| **Expressive** | Shift focus more toward the goal, less generalities. Be a little less casual | Listen more, take notes, don't try to outtalk each other | Slow your pace and focus more on the relationship | Focus on some detail in relation to the goals |
| **Amiable** | Shift Focus more toward the goal, less generalities | Pick up the pace, focus less on the relationship | Focus more on the issues at hand and less on popularity and networking | Focus less on the relationship and more on details | 
| **Analytical** | Pick up the pace and focus less on details and more on the goal | Focus a little less on detail. Be a little more causal | Focus a little more on the relationship. Be more casual | Try to look at ideas more objectively. You both need to be right. |

## Discover Your SOCIAL STYLE

Interested in learning about your own SOCIAL STYLE and those of your team? Below are five steps you can take as an individual contributor or a people manager to better understand your people. 

1. **Complete the Self-Assessment** - Discover yourself using this link to a [PDF version of the self-assessment](https://www.iths.org/wp-content/uploads/SELF-ASSESSMENT_OF_SOCIAL_STYLES.pdf). Download the PDF and fill in for yourself. Once you have completed the assessment, determine your score and associated style. 

2. **Reflection** - Reflect briefly on your result. What is your Social Style?

3. **Feedback** - Invite your colleaques to give feedback on your style.

4. **Share** - Share the assessment with teammates who have not taken the assessment.

5. **Aggregate Results** - Maintain a centralized location where exisiting and new team memebers can view individual social styles. 

## Example of putting styles into practice

Below is an example of Persona that is used to show how social styles can be put into practice as a Manager within GitLab. **Personas's are a tool to show a representation of the target audience**. In this example, we've applied the SOCIAL STYLES framework to a ficitious manager at GitLab and how they could use the assessment within their team to personalize a management approach. Personas are archetypical users whose goals and characteristics represent the needs of a larger group. In this example, Managers. However, we can use personas for any audience at GitLab and beyond.  

Use the persona as a roadmap to implementing the SOCIAL STYLEs assessment within your team. 

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vR6HxchUNNbwIYpZY2feMs8GFY080DaZZgxTZrZlCyo3QYgCPGD6oH68myqUFExrjta350ZVORwBG5E/embed?start=true&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

## Additional Resources on SOCIAL STYLES

Below are additional resources on social styles for review
*  [TRACOM SOCIAL STYLE Model](https://tracom.com/social-style-training/model)
*  [Social Styles and Conflict Resolution](https://legadima.co.za/conflict-management-social-styles/)
*  [Managerial Success Stories](https://tracom.com/wp-content/uploads/2019/01/Managerial-Success-Story-TRACOM_ss2018.pdf)
*  [Communicating with Style](https://www.wilsonlearning.com/wlw/articles/w/hidden-cost-comm/en-gb)


