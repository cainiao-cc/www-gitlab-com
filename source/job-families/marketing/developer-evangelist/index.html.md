---
layout: job_family_page
title: "Developer Evangelists"
---

As a Developer Evangelist, you will connect with other developers, contribute to open source, and share your work externally about cutting-edge technologies on conference panels, meetups, in contributed articles and on blogs. Your work will foster a community inspired by GitLab and will drive our strategy around developer love and GitLab’s participation in the open source ecosystem.
In Technical Evangelism, we collaborate and connect with communities that love technology and open source as much as we do. Our team interacts with developers across the globe at conferences as well as online, and co-creates with the open source community on the most impactful projects in the ecosystem. Our focus is on generating awareness about GitLab by rolling up our sleeves, contributing to the ecosystem, and enabling others to become evangelists outside the company as well. Not afraid to be hands-on, you might write sample code, author client libraries, and work with strategic GitLab partners such as the Heroes, users, and customers to spark and engage our developer communities.

## Responsibilities
* Lead the conversation around the latest technology advancements and best practices in the developer community at in person and online venues
* Contribute to relevant open source projects, foundations, and SIGs in order to give GitLab a voice and front seat access to the developments in our space of interest
* Channel information back to product and engineering about your learnings being an active contributor in the community
* Reach mass developers by creating unique content that educates the ecosystem and brings reflected glory to GitLab
* Conduct interviews with media via phone, podcasts, video and in-person
* Be a force in the community and never compromise on the tech!

## Requirements
* In-depth industry experience building software and contributing to open source in the cloud computing ecosystem
* At least 1 year of experience giving talks and developing demos, webinars, videos, and other technical content
* Meaningful social presence with engaged followers
* Ability to manage the fast moving conference schedule with it’s CFP deadlines and show dates
* Self-directed and work with minimal supervision.
* Outstanding written and verbal communications skills with the ability to explain and translate complex technology concepts into simple and intuitive communications.
* Ability to travel up to 40% of the time
* You share our values and work in accordance with those values.
* Ability to use GitLab

### Senior requirements

* Same as above plus,
* 2-3 year experience giving talks and developing demos, webinars, videos, and other technical content to audiences of 300 and larger
* Experience serving as a media spokesperson


### Staff requirements
* Same as above plus,
* Hold positions of influence in open source projects and organizations such as SIG leads, maintainer status, author status
* Social following of 10k+ followers or equivalent
* Experience giving talks and developing demos, webinars, videos, and other technical content as keynote speaker

## Specialities

### Kubernetes
* Experience using container technologies in general and Kubernetes in particular
* Contributor to Kubernetes or related projects such as Helm

### CI/CD
* Experience running or building CI/CD systems
* Contributor to a CI/CD project such as GitLab runners, Tekton, Spinnaker, Jenkins

### GitLab
* Deep knowledge of the GitLab product, particularly its CI/CD and Secure functionalities
* Ability to create engaging demos and other content based on GitLab that tell a story

### Ecosystem
* Deep connections in the cloud native ecosystem with demonstrated involvement in community events and projects
* Experience driving influencers to events, content, and other experiences
* Ability and experience nurturing community members and convert them to champions
* Partnerships experience to push relevant technical initiatives through the finish line keeping in mind holistic campaigns such as KubeCon events


## Hiring Process
* Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our team page.
* Qualified candidates will be invited to schedule a screening call with one of our Global Recruiters.
* Candidates will then be invited to schedule a 45 minute interview with the hiring manager, our Director of Technical Evangelism
* Candidates will then be invited to do a panel presentation on a topic of their choice.  There will be 20-30 minutes for the presentation, followed by 20-30 minutes for Q&A afterwards where the panel can ask the candidate questions and vice versa. Panelist interviewers will be the Director of Tech Evangelism, Snr. Director of Corporate Marketing, an Engineer, and a Product Manager
* Finally, our CEO or CMO may choose to conduct a final interview.
* Successful candidates will subsequently be made an offer via email.
* Additional details about our process can be found on our hiring page.

## Specialties
Read more about what a [specialty](/handbook/hiring/#definitions) is at GitLab here.

### Technical Evangelism Program Manager

_Reports to: Director of Technical Evangelism_

**Mission:** Build a powerhouse technical evangelism program at GitLab that encompasses teammates and the wider community. Create a program to increase GitLab’s awareness in the wider community.

As part of this role you will have the opportunity to work closely with deeply technical leaders and marketing experts. You will be coached to develop your own technical thought leadership platform and also have the opportunity to grow your career as a technical marketer.

**Responsibilities:**
* Develop speaker’s bureau database that includes speakers and thought leadership platforms
* Manage the conferences' Call For Proposals (CFP) process, working hand-in-hand with internal and external thought leaders on their talk submissions
* Review slide decks and develop a speaker training program
* Develop slide deck formats for speaker’s that include slides of interest to GitLab campaigns
* Support technical evangelism content development in line with thought leadership platforms
* Liaison with PR and content to execute on evangelism campaigns, as well as extend the effort of recorded talks at various events
* Work hand-in-hand with the community relations team to extend the evangelism program’s reach.
* Define metrics of measurement with the Tech Evangelism Director
* Instrument for metrics of measurement and report on them regularly
* Build thought leadership in GitLab product areas over time

## **Senior Responsibilities:**
* Uplevel the speaker's bureau to an influencer bureau and run the program with all modes of content creation in mind - speaking, technical demos, blog posts, media, etc.
* DRI integrated campaigns with other GitLab teams to achieve the above
* Create product <> tech evangelism liaison plan for future technical evangelist hires with Director of Tech Evangelism


**Requirements:**
* Excellent written and verbal communication skills
* Background in or deep curiosity about the cloud computing and devops ecosystem
* Track record of success in a software company
* At least 3-5 years work experience in a fast paced working environment
* Exceptional organizational skills
* Technical skills are a plus
* Relationships in the software DevSecOps space are a plus



